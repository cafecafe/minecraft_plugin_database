package speechless;

import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class main extends JavaPlugin {
    String host, port, database, username, password;
    public static Connection connection;
    @Override
    public void onEnable() {
        saveDefaultConfig();
        host = getConfig().getString("msql.host");
        getLogger().info(host);
        port = getConfig().getString("msql.port");
        database = getConfig().getString("msql.database");
        username = getConfig().getString("msql.username");
        password = getConfig().getString("msql.password");
        try {
            openConnection();
        }
        catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }
    public Connection openConnection() throws SQLException, ClassNotFoundException {
        if (connection != null && !connection.isClosed()) {
            return null;
        }
        Class.forName("com.mysql.jdbc.Driver");
        return connection = DriverManager.getConnection("jdbc:mysql://"
                        + this.host + ":" + this.port + "/" + this.database,
                this.username, this.password);
    }
}
