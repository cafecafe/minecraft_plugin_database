package speechless;

import org.bukkit.Bukkit;

import java.sql.*;
public class Table {
    private static speechless.main main;
    static  private Connection con = main.connection;
    static  private Statement stmt;

    static {
        try {
            stmt =  Table.con.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static boolean checkAndCreateTable(String sql,String name) throws SQLException {
        DatabaseMetaData dbm = con.getMetaData();
        Statement stmt = con.createStatement();
        ResultSet table = dbm.getTables(null, null, name, null);
        if (!table.next()) {
            stmt.executeUpdate(sql);
            Bukkit.getLogger().info("Created "+name+" in given database...");
            return true;
        }
        return false;
    }
    public static class Block_Table {
        public Block_Table(){
            String sql = "CREATE TABLE blocks" +
                    "(id INTEGER not NULL AUTO_INCREMENT, " +
                    " block_id VARCHAR(255), " +
                    " owner VARCHAR(255), " +
                    " PRIMARY KEY ( id ))";
            try {
                checkAndCreateTable(sql, "blocks");
            }
            catch (SQLException e){
                e.printStackTrace();
            }
        }
        public boolean registerOwner(String block_id,String owner){
            try {
                if(stmt.executeUpdate("UPDATE blocks SET"
                        +" owner='"+owner+"' where block_id = '"+block_id+"'") == 0){
                    //                    Bukkit.getLogger().info("create");
                    stmt.executeUpdate("INSERT into blocks(block_id,owner)"+
                            "values('"+block_id+"' ,'"+owner+"')",Statement.RETURN_GENERATED_KEYS);
                    return false;

                }
                else{
                    return true;
                }
            }
            catch(SQLException e){
              Bukkit.getLogger().info(e.getMessage());
            }
            return  false;
        }
        public String getOwner(String block_id){
            try {
                ResultSet result = stmt.executeQuery("SELECT * from blocks " +
                        "where block_id = '" + block_id + "'" );
                if(!result.next()) return "";
                return result.getString("owner");
            }
            catch (SQLException e){
                e.printStackTrace();
            }
            return null;
        }

    }
    public static class Score_Table {
        public Score_Table(){
            String sql =  "CREATE TABLE scores" +
                    "(id INTEGER not NULL AUTO_INCREMENT, " +
                    " name VARCHAR(255), " +
                    " score INTEGER(255), " +
                    " stage INTEGER(2)" +
                    " PRIMARY KEY ( id ))";
            try {
                checkAndCreateTable(sql, "scores");
            }
            catch (SQLException e){
                e.printStackTrace();
            }
        }
        public void setScore(String name, int i) {
            try {
                String sql = "UPDATE scores SET  score =" + i
                        + " where name ='" + name + "'";
                if (stmt.executeUpdate(sql) == 0) {
                    sql = "INSERT into scores(name,score) values('" + name + "'," + i + ")";
                    stmt.executeUpdate(sql);
                }
            } catch (SQLException e) {

            }
        }
        public  int getScore(String name){
            try{
                String sql = "SELECT * from scores where name = '"+name+"'";
                ResultSet result =stmt.executeQuery(sql);
                if(result.next()){
                    return result.getInt("score");
                }
                else{
                    return 0;
                }
            }catch(SQLException e){
                e.printStackTrace();
            }
            return 0;
        }
        public int getIntField(String field , String name){
            try{
                String sql = "SELECT "+field+" from scores where name = '"+name+"'";
                ResultSet result =stmt.executeQuery(sql);
                if(result.next()){
                    return result.getInt(field);
                }
            }catch(SQLException e){
                e.printStackTrace();
            }
            return 0;
        }
        public void setIntField(String field , int i,String name){
            try {
                String sql = "UPDATE scores SET  "+field+" =" + i
                        + " where name ='" + name + "'";
                if (stmt.executeUpdate(sql) == 0) {
                    sql = "INSERT into scores(name,"+field+") values('"+name+"',"+i+")";
                    stmt.executeUpdate(sql);
                }
            } catch (SQLException e) {

            }
        }
    }

}
